import React from "react";
import { Link } from 'react-router-dom';
import { Card, CardTitle, Row, Col } from "react-materialize";

const Cartas = () => {
  return (
    <div>
      <Row>
        <Col s={4} key='1'>
        
          <Card
            className="small"
            header={<CardTitle image="../images/all.jpg"></CardTitle>}
            actions={<Link className='purple-text' to = 'all'>Todos los personajes</Link>}
          >
            busca los personajes de Rick y morty
          </Card>
        </Col>
       
        <Col s={4} key='2'>
          <Card
            className="small"
            header={<CardTitle image="../images/Rick-and-Morty.jpg"></CardTitle>}
            actions={<Link className=' purple-text' to = '/favs'> Mis favoritos</Link>}
          >
            Dale una checada a los que has guardado
          </Card>
        </Col>
        <Col s={4} key='3'>
          <Card
            className="small"
            header={<CardTitle image="../images/random.jpg"></CardTitle>}
            actions={<Link  className='purple-text' to='/random'>Random</Link>}
          >
            Echa la suerte y picale al Random
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default Cartas;
