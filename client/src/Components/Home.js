import React from 'react';
import Nav from './Nav';
import Central from './Central';
import Foot from './Foot';

const Home = () => {
  return (
    <div>
      <Nav/>
      <Central/>
      <Foot/>
    </div>
  )
}

export default Home
