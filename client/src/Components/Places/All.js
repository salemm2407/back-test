import React, { Component } from "react";
import Nav from "../Nav";
import { Link } from "react-router-dom";
import { Row, Col, Card, CardTitle, Button, Icon } from "react-materialize";
import { allCharacters } from "../../lib/all";

class All extends Component {
  state = {
    characters: []
  };

  componentWillMount() {
    allCharacters()
    .then(response => {
      console.log(response.data.results)
      const { results } = response.data
      this.setState({characters:results})
    })
  }
  render() {
    return (
      <div>
        <Nav />
        <div className="container">
          <Row>
            {this.state.characters.map(character => {
              return (
                <Col s={12} m={3} key={character.id}>
                  <Card
                    
                    className="small"
                    header={
                      <CardTitle image={character.image}>
                        {character.name}
                      </CardTitle>
                    }
                    actions={<Link to={`/single/${character.id}`}>detalle</Link>}
                  >
                    <p>{character.status}</p>
                  </Card>
                </Col>
              );
            })}
          </Row>
          <Row>
            <Col>
              <Button   waves="light" className="black white-text">
                {" "}
                <Icon right></Icon>
                Guardar personaje
              </Button>
            </Col>
            
            <Col>
              <Link to="/all">
                <Button waves="light" className="black white-text">
                  {" "}
                  <Icon right>cancel</Icon>
                  Regresar
                </Button>
              </Link>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export default All;
