import React, { Component } from 'react';
import axios from 'axios';
import Nav from '../Nav';
import { Link } from "react-router-dom";
import { Row, Col, Card, CardTitle, Button, Icon } from "react-materialize";
class Favs extends Component {
    state={
        favs:[],
        error:''
    }
     async componentDidMount(){
      try{
       const favs = await axios.get('http://localhost:3000/favs')
       
       const { data } = favs
       this.setState({favs:data})
      }catch(error){
        this.setState({error})
      }
    }

  render() {
      console.log(this.state)
    return (
      <div>
        <Nav/>
        <div className='container'>
        {(this.state.favs.length === 0)?(<Row><h1>Aun no tienes favoritos</h1></Row>):
        (
            <Row>
           {this.state.favs.map(fav =>{
             return (
              <Col s={12} m={3} key={fav.id}>
                  <Card
                    
                    className="small"
                    header={
                      <CardTitle image={fav.image}>
                        {fav.name}
                      </CardTitle>
                    }
                    actions={<Link to={`/single/${fav.id}`}>detalle</Link>}
                  >
                    <p>{fav.status}</p>
                  </Card>
                </Col>
             )
           })}
        </Row>
        )}
        <Row>
            
            <Col>
              <Link to="/random">
                <Button waves="light" className="black white-text">
                  {" "}
                  <Icon right>autorenew</Icon>
                  Randoms
                </Button>
              </Link>
            </Col>
            <Col>
              <Link to="/">
                <Button waves="light" className="black white-text">
                  {" "}
                  <Icon right>cancel</Icon>
                  Regresar
                </Button>
              </Link>
            </Col>
          </Row>

        </div>
      </div>
    )
  }
}
export default Favs