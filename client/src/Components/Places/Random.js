import React, { Component } from 'react'
import Nav from '../Nav'
import { randomChar } from '../../lib/random';
import { Row, Button, Icon, Col } from "react-materialize";
import { saveChar } from '../../lib/saveChar';
import {Link} from 'react-router-dom'

class Random extends Component {
  state={
    character: {},
    id:0,
    image:'',
    name:'',
    status:'',
    species:'',
    gender:''
  }
 componentWillMount(){
   randomChar()
   .then(response => {
     const {data} = response
     this.setState({character: data, id:data.id, image:data.image,name:data.name, status:data.status,species:data.species,gender:data.gender})
   })
 }
 refresh = ()=> {
  randomChar()
  .then(response => {
    const {data} = response
    this.setState({character: data, id:data.id, image:data.image,name:data.name, status:data.status,species:data.species,gender:data.gender})
  })
 }
 submit = event => {
  event.preventDefault()
 saveChar(this.state)

}
  render() {
    return (
      <div>
        <Nav/>
        <Row>
         <Col m={5}>
         <Row className="container">
         <img src={this.state.character.image} alt='imagen'></img>
         </Row>
         <div>
            
         </div>
         </Col>

         <Col m={7}>
            <div className='container'>
              <ul>
                <li>Nombre:{this.state.character.name}</li>
                <li>Estatus:{this.state.character.status}</li>
                <li>Especie:{this.state.character.species}</li>
                <li>Genero:{this.state.character.gender}</li>
                {/* <li>Origen:{this.state.character.origin.map(origen =>{return <li>{origen.name}</li>})}</li> */}
              </ul>
            </div>
         </Col>
        </Row>
        <Row>
            <Col>
              <Button   onClick={this.submit} type="submit" waves="light" className="black white-text">
                {" "}
                <Icon right>person_add</Icon>
                Guardar personaje
              </Button>
            </Col>
            <Col>
              <Button  onClick={this.refresh} type='submit' waves="light" className="black white-text">
                {" "}
                <Icon right>autorenew</Icon>
                Siguiente Random
              </Button>
            </Col>
            <Col>
              <Link to="/">
                <Button waves="light" className="black white-text">
                  {" "}
                  <Icon right>cancel</Icon>
                  Regresar
                </Button>
              </Link>
            </Col>
            <Col>
              <Link to="/favs">
                <Button waves="light" className="black white-text">
                  {" "}
                  <Icon right>favorites</Icon>
                  favoritos
                </Button>
              </Link>
            </Col>
          </Row>
      </div>
    )
  }
}
export default Random
