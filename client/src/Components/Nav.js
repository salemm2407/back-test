import React from 'react'
import { Navbar, NavItem } from 'react-materialize';


const Nav = () => {
    return (
        <div>
            <Navbar brand='Common Sense BackTest' className='black' right>
                <NavItem >Consulta de API</NavItem>
                <NavItem >Rick and Morty</NavItem>
            </Navbar>
        </div>
    )
}

export default Nav
