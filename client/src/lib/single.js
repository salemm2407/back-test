import axios from 'axios';
const baseURL  = 'http://localhost:3000/';

export const singleChar = (charID) => {
    return axios.get(`${baseURL}single/${charID}`)
}