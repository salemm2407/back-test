import axios from 'axios'
import swal from 'sweetalert2'

const baseURL = 'http://localhost:3000/'

export  const saveChar = (data) => {
   axios.post(`${baseURL}saveChar`,data)
   .then( save => {
     
    swal({
        type: 'success',
        title: 'Agregaste un nuevo Favorito',
        text: save.data.newChar.name
      })
   })
   .catch(error => {
    swal({
      type: 'error',
      title: 'Ya agregaste previo a este personaje',
      text: error
    })
   })
}