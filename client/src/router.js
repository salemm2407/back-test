import React from 'react';
import {
    BrowserRouter as Router,
    Route
} from 'react-router-dom';

import Home from './Components/Home'
import Single from './Components/Places/Single';
import All from './Components/Places/All';
import Random from './Components/Places/Random';
import Favs from './Components/Places/Favs';
export default () => <Router>
    <div>
        <Route exact path = '/' component={Home} />
        <Route path ='/single/:charID' component={Single}/>
        <Route path ='/all' component={All}/>
        <Route path = '/random' component={Random}/>
        <Route path = '/favs' component={Favs}/> 
    </div>
</Router>