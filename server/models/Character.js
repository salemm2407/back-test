const mongoose = require ('mongoose')
const Schema = mongoose.Schema;

const characterSchema = new Schema ({
    id: Number,
    image:String,
    name: String,
    status: String,
    species: String,
    gender: String
    }, {
        timestamps: {
          createdAt: 'created_at',
          updatedAt: 'updated_at'
        }

})

const Character = mongoose.model('Character',characterSchema);
module.exports = Character;