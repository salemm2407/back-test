const express = require ('express');
const router  = express.Router();
const axios   = require('axios')
const Character = require('../models/Character')
const baseUrl = 'https://rickandmortyapi.com/api/character/'

//consulta todos los caracteres
router.get('/all',  async (req,res,next) => {
  try{
    const response = await axios.get(baseUrl)
    console.log('llego la pregunta')
        return res.status(200).json(response.data)
  }catch(error){
        res.status(400).json({error:error})
  }
  
})
//UN SOLO PERSONAJE POR ID
router.get('/single/:id', async (req, res, next) => {
  try{
    console.log('este es el id',req.params.id)
    const characterId = req.params.id
    const response = await axios.get(baseUrl+characterId)
         res.status(200).json(response.data)
  }catch(error){
    res.status(400).json({error:error})
  }
})
//personajes random
router.get('/random', async (req, res, next) => {
  try{
   
    const selected = Math.floor(Math.random()* 394)+1
    const random = await axios.get(baseUrl+selected)
    res.status(200).json(random.data)
  }catch(error){
    res.status(400).json({error:error})
  }
})

router.post('/saveChar', async (req,res,next) => {
  try{
    console.log(req.body)
    const {id, image, name, status, species, gender } = req.body
    // Character.findOne({name},"name",(err, nam) => {
    //   if(nam !== null){
    //     res.status(403).json({ 
    //       route: "saveChar",  
    //       message: "favorite already exists" 
    //   })
    //   return
    // }
    // })
   
    const newChar = new Character({id, image, name, status, species, gender})
    newChar.save()
    await res.status(200).json({newChar})
    console.log('has salvado')
  }catch(error){
    res.status(403).json('opss! algo ha salido mal', error)
  }
})

router.get('/favs', async (req,res,next) => {
  try{
   const chars = await Character.find()
   res.status(200).json(chars)
  }catch(error){
      res.status(400).json({error:error})
  }
})
module.exports = router