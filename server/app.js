require('dotenv').config();

const bodyParser   = require('body-parser');
const cookieParser = require('cookie-parser');
const express      = require('express');
const mongoose     = require('mongoose');
const logger       = require('morgan');
const cors         = require('cors'); 

mongoose
  .connect('mongodb://localhost/rickmorty', {useNewUrlParser: true})
  .then(x => {
    console.log(`Connected to Mongo! Database name: "${x.connections[0].name}"`)
  })
  .catch(err => {
    console.error('Error connecting to mongo', err)
  });


const app = express();
app.use(cors())
// Middleware Setup
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

      
const index = require('./routes/index');
app.use('/', index);

const rickApi = require ('./routes/rickApi');
app.use('/', rickApi);

 const PORT = process.env.PORT || 3000
 app.listen(PORT, console.log('listen on ${PORT}'))
module.exports = app;
